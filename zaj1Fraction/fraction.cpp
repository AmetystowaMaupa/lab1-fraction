#include <iostream>
#include <cstring>
#include <cctype>

using namespace std;

#include "fraction.h"

#ifdef UNIMPLEMENTED_classFraction
#ifdef _MSC_FULL_VER // if Visual Studio Compiler
    #pragma message ("Klasa jest do zaimplementowania. Instrukcja w pliku naglowkowym")
#else
    #warning "Klasa jest do zaimplementowania. Instrukcja w pliku naglowkowym"
#endif // #ifdef _MSC_FULL_VER
#endif // #ifdef UNIMPLEMENTED_classFraction
